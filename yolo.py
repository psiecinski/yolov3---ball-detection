import numpy as np
import argparse
import cv2 as cv
import subprocess
import os
import pyrealsense2 as rs
from yolo_utils import infer_image, show_image, calculate_corner_pixel
import json
import requests
from conf import CONF

FLAGS = []
ratio_x = 1
ratio_y = 1

if __name__ == '__main__':
	cameraid = CONF["cameraid"]
	labels = open(CONF["labels"]).read().strip().split('\n')
	colors = np.random.randint(0, 255, size=(len(labels), 3), dtype='uint8')
	net = cv.dnn.readNetFromDarknet(CONF["config"], CONF["weights"])
	layer_names = net.getLayerNames()
	layer_names = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]
	count = 0
	pipeline = rs.pipeline()
	config = rs.config()
	config.enable_stream(rs.stream.depth, CONF["camera_width"], CONF["camera_height"], rs.format.z16, 30)
	config.enable_stream(rs.stream.color, CONF["camera_width"], CONF["camera_height"], rs.format.bgr8, 30)
	pawel = pipeline.start(config)
	depth_sensor = pawel.get_device().first_depth_sensor()

	while True:
		frames = pipeline.wait_for_frames()
		depth_frame = frames.get_depth_frame()
		color_frame = frames.get_color_frame()
		color = np.asanyarray(color_frame.get_data())
		if not depth_frame or not color_frame:
			continue

		depth_image = np.asanyarray(depth_frame.get_data())
		frame = color
		align = rs.align(rs.stream.color)
		frames = align.process(frames)
		
		if count == 0:
			frame, boxes, confidences, classids, idxs, points, = infer_image(net, layer_names, \
								CONF["camera_height"], CONF["camera_width"], frame, colors, labels, FLAGS)
			count += 1
		else:
			frame, boxes, confidences, classids, idxs, points = infer_image(net, layer_names, \
								CONF["camera_height"], CONF["camera_width"], frame, colors, labels, FLAGS, boxes, confidences, classids, idxs, infer=False)
			count = (count + 1) % 6
	
		if boxes:
			corners = []
			detected_objects = []
			align = rs.align(rs.stream.color)
			frames = align.process(frames)
			depth_scale = depth_sensor.get_depth_scale()
			toSend = []
			for point in points:
				if point["classid"] == 0:
					corners.append(point)
				else:
					detected_objects.append(point)

			if len(corners) == 4:					
				side_pixels_x, side_pixels_y, min_x, max_y = calculate_corner_pixel(corners)		#PIXELS BETWEEN CORNER
				ratio_x = CONF["TABLE_SIDE_LENGTH"] / side_pixels_x	if side_pixels_x != 0 else 1
				ratio_y = CONF["TABLE_SIDE_LENGTH"] / side_pixels_y	if side_pixels_y != 0 else 1
				print(side_pixels_x, side_pixels_y, ratio_x, ratio_y)

			if ratio_x != 1 and ratio_y != 1:
				for point in detected_objects:
					x, y = point["x"], point["y"] 
					translated_x = ((x - min_x) * ratio_x) - 250
					translated_y = ((y - max_y) * -1) * ratio_y
					print(translated_x, translated_y)
					depth = depth_image[point['y'],point['x']].astype(float)
					distance = depth * depth_scale
					obj = {"id": ("block", labels[point["classid"]], 1), "coords": (translated_x, translated_y, distance), "text": "cos"}
					print(obj)
					toSend.append(obj)

				if len(toSend) > 0:
					print(toSend)
					url = 'http://192.168.43.139:5000/vision'
					objectsArray = json.dumps(toSend)
					headers = {'content-type': 'application/json'}
					r = requests.post(url, data=objectsArray, headers=headers)
		
		depth_frame = frames.get_depth_frame()
		colorizer = rs.colorizer()
		colorized_depth = np.asanyarray(colorizer.colorize(depth_frame).get_data())
		images = np.hstack((color, colorized_depth))
		cv.namedWindow('RealSense', cv.WINDOW_AUTOSIZE)
		cv.imshow('RealSense', images)
		if cv.waitKey(1) & 0xFF == ord('q'):
			break
	cv.destroyAllWindows() 