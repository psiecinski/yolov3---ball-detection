CONF = {
	"weights" : './yolov3-coco/yolov3.weights',
	"cameraid" : 0,
	"labels" : './yolov3-coco/coco-labels',
	"config" : './yolov3-coco/yolov3.cfg',
	"image_path" : "",
	"threshold" : 0.3,
    "camera_width" : 640,
    "camera_height" : 480,
    "TABLE_SIDE_LENGTH" : 500,
    "ratio_x" : 1,
    "ratio_y" : 1,
    "confidence" : 0.5
}