import numpy as np
import argparse
import cv2 as cv
import subprocess
import time
import os
from conf import CONF
points_array = []

def show_image(img):
    cv.imshow("Image", img)
    cv.waitKey(0)

def draw_labels_and_boxes(img, boxes, confidences, classids, idxs, colors, labels):
    # If there are any detections
    point = {}
    if len(idxs) > 0:
        for i in idxs.flatten():
            # Get the bounding box coordinates
            x, y = boxes[i][0], boxes[i][1]
            w, h = boxes[i][2], boxes[i][3]
            cx = int(x + w/2)
            cy = int(y + h/2)
            point = {'x': cx, 'y': cy}
            #print(point)
            # Get the unique color for this class
            color = (1, 190, 200)
            color2 = (1, 190, 200)
            # Draw the bounding box rectangle and label on the image
            cv.rectangle(img, (x, y), (x+w, y+h), color, 2)
            cv.rectangle(img, (cx, cy), (cx, cy), color2, 2)
            text = "{}: {:4f}".format(labels[classids[i]], confidences[i])
            txtadd = " centre: (" + str(x + w/2) + "," + str(y + h/2) + ")"
            cv.putText(img, text, (x, y-5), cv.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
            cv.putText(img, txtadd, (cx, cy-5), cv.FONT_HERSHEY_SIMPLEX, 0.5, color2, 2)
    obj = [img, point]            
    return obj


def generate_boxes_confidences_classids(outs, height, width, tconf):
    boxes = []
    confidences = []
    classids = []

    for out in outs:
        for detection in out:
            
            # Get the scores, classid, and the confidence of the prediction
            scores = detection[5:]
            classid = np.argmax(scores)
            confidence = scores[classid]
            
            # Consider only the predictions that are above a certain confidence level
            if confidence > tconf:
                # TODO Check detection
                box = detection[0:4] * np.array([width, height, width, height])
                centerX, centerY, bwidth, bheight = box.astype('int')
                x = int(centerX - (bwidth / 2))
                y = int(centerY - (bheight / 2))
                # Append to list
                boxes.append([x, y, int(bwidth), int(bheight)])
                confidences.append(float(confidence))
                classids.append(classid)
    
    saved_points = set_points(boxes, classids, confidences)
    return boxes, confidences, classids, saved_points


    
def set_points(boxes, classids, confidences):
    points_array = []
    for i in range(len(boxes)):
        x, y = boxes[i][0], boxes[i][1]
        w, h = boxes[i][2], boxes[i][3]
        cx = int(x + w/2)
        cy = int(y + h/2)
        points = {"classid":classids[i],'x': cx, 'y': cy, "confidence": confidences[i]}
        points_array.append(points)
    return points_array


def infer_image(net, layer_names, height, width, img, colors, labels, FLAGS, 
            boxes=None, confidences=None, classids=None, idxs=None, infer=True):
    
    saved_points = []
    if infer:
        # Contructing a blob from the input image
        blob = cv.dnn.blobFromImage(img, 1 / 255.0, (416, 416), 
                        swapRB=True, crop=False)

        # Perform a forward pass of the YOLO object detector
        net.setInput(blob)

        # Getting the outputs from the output layers
        start = time.time()
        outs = net.forward(layer_names)
        end = time.time()
        
        # Generate the boxes, confidences, and classIDs
        boxes, confidences, classids, saved_points = generate_boxes_confidences_classids(outs, height, width, CONF["confidence"])
        # Apply Non-Maxima Suppression to suppress overlapping bounding boxes
        idxs = cv.dnn.NMSBoxes(boxes, confidences, CONF["confidence"], CONF["threshold"])

    if boxes is None or confidences is None or idxs is None or classids is None:
        raise '[ERROR] Required variables are set to None before drawing boxes on images.'
        
    # Draw labels and boxes on the image
    img = draw_labels_and_boxes(img, boxes, confidences, classids, idxs, colors, labels)
    return img[0], boxes, confidences, classids, idxs, saved_points


def calculate_corner_pixel(array):
    #print("pre: ", array)
    array.sort(key=lambda a: a["x"])
    #print("post: ", array)
    difference_x = abs(array[-1]["x"] - array[0]["x"])
    #print("difference_x: ", difference_x)
    min_x = array[0]["x"]
    array.sort(key = lambda a: a["y"])
    max_y = array[-1]["y"]
    difference_y = abs(array[-1]["y"] - array[0]["y"])
    #print("difference_y: ", difference_y)
    return difference_x, difference_y, min_x, max_y
